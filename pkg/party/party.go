package party

import (
	"fmt"

	"github.com/lib/pq"
	"gorm.io/gorm"
)

type Party struct {
	ID        int
	Name      string
	Manifesto string
	LeaderID  int
}

type PartiesPlayer struct {
	ID       int
	PlayerID int
	PartyID  int
}

func IsMemberOfAnyParty(db *gorm.DB, ID int) bool {
	var p PartiesPlayer
	db.Table("parties_players").Where("player_id = ?", ID).First(&p)
	return p.ID != 0
}

func FindForPlayerID(db *gorm.DB, playerID int) *Party {
	var party Party

	db.Raw(`
		SELECT
			p.*
		FROM
			parties_players pp
		INNER JOIN
			parties p
		ON
			pp.party_id =  p.id
		WHERE
			player_id = ?
	`, playerID).Scan(&party)

	return &party
}

func FindByID(db *gorm.DB, ID int) *Party {
	var p Party
	_ = db.Table("parties").Where("id = ?", ID).First(&p)
	return &p
}

func Find(db *gorm.DB, name string) *Party {
	var p Party
	_ = db.Table("parties").Where("name = ?", name).First(&p)
	return &p
}

func ApprovedParties(db *gorm.DB) []Party {
	var parties []Party
	_ = db.Where("approved = true").Find(&parties)
	fmt.Printf("parties = %+v\n", parties)
	return parties
}

type PartiesAndMembers struct {
	ID        int
	Name      string
	Manifesto string
	Members   pq.StringArray `gorm:"type:text[]"`
}

// We aren't get the parties that have no members
func AllPartiesAndMembers(db *gorm.DB) []PartiesAndMembers {
	var res []PartiesAndMembers
	db.Raw(`
		SELECT
			pa.id, pa.name, pa.manifesto, array_agg(p.name) as members
		FROM
			parties pa
		LEFT JOIN
			parties_players pp
		ON
			pa.id = pp.party_id
		LEFT JOIN
			players p
		ON
			pp.player_id = p.id
		GROUP BY
			pa.id, pa.name, pa.manifesto
	`).Scan(&res)

	return res
}

func AllParties(db *gorm.DB) []Party {
	var parties []Party
	_ = db.Find(&parties)
	fmt.Printf("parties = %+v\n", parties)
	return parties
}

type LeavePartyResult struct {
	ID   int
	Name string
}

func LeaveParty(db *gorm.DB, playerID int) (*LeavePartyResult, error) {
	var res LeavePartyResult

	tx := db.Raw(`
		SELECT
			p.id, p.name
		FROM
			parties_players pp
		INNER JOIN
			parties p
		ON
			pp.party_id = p.id
		WHERE
			pp.player_id = ?
		`, playerID).Scan(&res)

	fmt.Printf("\t LEAVING PARTY: %+v\n", res.ID)
	tx = db.Table("parties_players").Where("party_id = ?", res.ID).Delete(&PartiesPlayer{})
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}

	return &res, tx.Error
}

func JoinParty(db *gorm.DB, partyName string, playerID int) error {
	party := Find(db, partyName)
	pp := PartiesPlayer{
		PlayerID: playerID,
		PartyID:  party.ID,
	}
	tx := db.Create(&pp)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}
	fmt.Printf("pp = %+v\n", pp)
	return nil
}

func (p *Party) MemberNames(db *gorm.DB) []string {
	var memberNames []string
	db.Raw(`
		SELECT
			p.Name
		FROM
			parties_players pp
		INNER JOIN
			players p
		ON
			pp.player_id = p.id
		WHERE
			pp.party_id = ?
	`, p.ID).Scan(&memberNames)

	return memberNames
}
