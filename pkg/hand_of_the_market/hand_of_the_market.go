package hand_of_the_market

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gorm.io/gorm"
)

// TODO: Make this configurable
func Serve(ctx context.Context, db *gorm.DB) <-chan string {
	fmt.Println("Hand of the Market Started")

	results := make(chan string)
	ticker := time.NewTicker(5 * time.Minute)

	moleTicker := time.NewTicker(15 * time.Minute)
	// host := "us-east-1.linodeobjects.com"
	// db.Table("stream_jesters").Exec(`UPDATE stream_jesters SET deleted_at = NOW()`)

	go func() {
		db.Table("players").Where("mana < 3").Update("mana", 3)
		chat.GiveStreetCredToRecentChatters(db)
		results <- "GlitchCat CoolCat CoolCat GlitchCat Mana Replenished GlitchCat CoolCat CoolCat GlitchCat "
		// results <- "CoolCat CoolCat CoolCat Mana replenished"

		// stream_jester.NewSecret(db)
		// results <- fmt.Sprintf("Use the credentials to find the secret and play the sound first. If you win you become stream jester")
		// ac, err := pusher.CreateAndDeleteKeys(db, "read_write")
		// if err == nil {
		// 	leak := fmt.Sprintf("Leak: %s %s %s", ac.Name, ac.AccessKey, ac.SecretKey)

		// 	putLeak := fmt.Sprintf(`s3cmd put --acl-public --access_key=%s --secret_key=%s --host-bucket=%s --host=%s INSERT_LOCAL_FILE s3://%s/INSERT_FILENAME`,
		// 		ac.AccessKey, ac.SecretKey, host, host, ac.Name)
		// 	getLeak := fmt.Sprintf(`s3cmd get --recursive  --access_key=%s --secret_key=%s --host-bucket=%s --host=%s s3://%s/`,
		// 		ac.AccessKey, ac.SecretKey, host, host, ac.Name)
		// 	results <- putLeak
		// 	results <- getLeak
		// 	results <- leak
		// }

		for {
			select {
			case <-ctx.Done():
				return
			case _ = <-moleTicker.C:
				// stream_jester.NewSecret(db)
				// ac, err := pusher.CreateAndDeleteKeys(db, "read_only")
				// if err == nil {
				// 	// leak := fmt.Sprintf("Leak: %s %s %s", ac.Name, ac.AccessKey, ac.SecretKey)
				// 	niceLeak := fmt.Sprintf(`s3cmd get --recursive  --access_key=%s --secret_key=%s --host-bucket=%s --host=%s s3://%s/`,
				// 		ac.AccessKey, ac.SecretKey, host, host, ac.Name)
				// 	results <- niceLeak
				// 	// results <- leak
				// }
			case _ = <-ticker.C:
				db.Table("players").Where("mana < 3").Update("mana", 3)
				chat.GiveStreetCredToRecentChatters(db)

				results <- "GlitchCat CoolCat CoolCat GlitchCat Mana Replenished GlitchCat CoolCat CoolCat GlitchCat "
			}
		}
	}()

	return results
}
