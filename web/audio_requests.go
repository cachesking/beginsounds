package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
)

type AudioRequestsPage struct {
	Metadata
	Domain            string
	Extension         string
	TotalPlayedSounds int64
	MostPopular       []audio_request.PlayCount
}

func AudioRequestHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	// With the Go, when the function doesn't
	// exist it doesn't auto-import
	var total_played int64
	var most_popular []audio_request.PlayCount
	total_played = audio_request.TotalPlayedSounds(db)
	most_popular = audio_request.MostPopularSounds(db)

	remotePage := AudioRequestsPage{
		Domain:            "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension:         ".html",
		Metadata:          metadata,
		TotalPlayedSounds: total_played,
		MostPopular:       most_popular,
	}
	buildFile := fmt.Sprintf("build/audio_requests.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = audioRequestsTmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := AudioRequestsPage{
		Metadata:          metadata,
		TotalPlayedSounds: total_played,
		MostPopular:       most_popular,
	}
	audioRequestsTmpl.Execute(w, page)
}
