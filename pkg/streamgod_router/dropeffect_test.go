package streamgod_router

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

// // TODO: Come Back
// func TestDropeffectRoute(t *testing.T) {
// 	ctx := context.Background()
// 	db := database.CreateDBConn("test_beginsounds3")
// 	test_support.ClearDb(db)

// 	cmdName := "damn"
// 	commands := make(chan chat.ChatMessage, 1)
// 	sc := stream_command.CreateFromName(db, cmdName)
// 	p1 := player.Player{Name: "beginbot", Streamgod: true}
// 	p2 := player.Player{Name: "young.thug"}
// 	db.Create(&p2)
// 	fmt.Printf("p2 = %+v\n", p2)
// 	fmt.Printf("sc = %+v\n", sc)

// 	msg := "!dropeffect damn young.thug"
// 	cm := chat.ChatMessage{
// 		PlayerID:   &p1.ID,
// 		PlayerName: p1.Name,
// 		Streamgod:  true,
// 		Message:    msg,
// 	}

// 	commands <- cm
// 	results, _, _ := Route(ctx, db, commands)

// 	select {
// 	case <-time.After(time.Millisecond * 50):
// 		t.Error("Did not receive a message")
// 	case res := <-results:
// 		expectedMsg := fmt.Sprintf("@young.thug now has !%s", cmdName)
// 		if res != expectedMsg {
// 			t.Errorf("Expected: %s | Received: %s", expectedMsg, res)
// 		}

// 		if !sc.IsAllowedToPlay2(db, p2.Name) {
// 			t.Errorf("We expected @young.thug to be allowed to play !damn")
// 		}
// 	}

// }
// !dropeffect @young.thug
func TestDropeffectToSpecificUser(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	chatResults := make(chan string, 1000)
	botbotResults := make(chan string, 1000)
	websocketResults := make(chan string, 1000)
	defer close(websocketResults)
	defer close(chatResults)
	defer close(botbotResults)

	cmdName := "damn"
	sc := stream_command.CreateFromName(db, cmdName)
	p1 := player.Player{Name: "beginbot", Streamgod: true}
	p2 := player.Player{Name: "young.thug"}
	db.Create(&p1)
	db.Create(&p2)

	parsedCmd := chat.ParsedCommand{
		Name:       "dropeffect",
		Username:   "beginbot",
		TargetUser: "young.thug",
		Streamlord: true,
		Streamgod:  true,
	}

	dropeffect(db, &parsedCmd, 1, chatResults, botbotResults, websocketResults)

	select {
	case <-time.After(50 * time.Millisecond):
		t.Errorf("We did not receive a message")
	case res := <-chatResults:
		expectedMsg := fmt.Sprintf("@young.thug got SFXs: !%s", cmdName)
		if res != expectedMsg {
			t.Errorf("Wrong Msg: %s", res)
		}

		if !sc.IsAllowedToPlay2(db, p2.Name) {
			t.Errorf("Expected: %s | Received: %s", expectedMsg, res)
		}
	}

}

// !dropeffect @young.thug 20
func TestDropeffectMultipleEffectsToSpecificUser(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	chatResults := make(chan string, 1000)
	botbotResults := make(chan string, 1000)
	websocketResults := make(chan string, 1000)
	defer close(websocketResults)
	defer close(chatResults)
	defer close(botbotResults)

	cmdName := "damn"
	sc := stream_command.CreateFromName(db, cmdName)
	cmdName2 := "hello"
	sc2 := stream_command.CreateFromName(db, cmdName2)
	p1 := player.Player{Name: "beginbot", Streamgod: true}
	p2 := player.Player{Name: "young.thug"}
	db.Create(&p1)
	db.Create(&p2)

	fmt.Printf("sc = %+v\n", sc)
	fmt.Printf("sc2 = %+v\n", sc2)

	parsedCommand := chat.ParsedCommand{
		Name:       "dropeffect",
		Username:   "beginbot",
		TargetUser: "young.thug",
		Streamlord: true,
		Streamgod:  true,
	}

	dropeffect(db, &parsedCommand, 2, chatResults, botbotResults, websocketResults)

	select {
	case <-time.After(50 * time.Millisecond):
		t.Errorf("We did not receive a message")
	case res := <-chatResults:
		fmt.Printf("res = %+v\n", res)
		// expectedMsg := fmt.Sprintf("@young.thug now has !%s", "damn")
		// if res != expectedMsg {
		// 	t.Errorf("Error wrong return: %s", res)
		// }

		if !sc.IsAllowedToPlay2(db, p2.Name) {
			t.Errorf("@%s should be allowed to play !%s", p2.Name, sc.Name)
		}
		if !sc2.IsAllowedToPlay2(db, p2.Name) {
			t.Errorf("@%s should be allowed to play !%s", p2.Name, sc2.Name)
		}
	}
}

// // !dropeffect 10
func TestDropeffectRandomEffectsToRandomUsers(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	chatResults := make(chan string, 1000)
	botbotResults := make(chan string, 1000)
	websocketResults := make(chan string, 1000)
	defer close(websocketResults)
	defer close(chatResults)
	defer close(botbotResults)

	cmdName := "damn"
	cmdName2 := "hello"
	sc := stream_command.CreateFromName(db, cmdName)
	sc2 := stream_command.CreateFromName(db, cmdName2)
	p1 := player.Player{Name: "beginbot", Streamgod: true}
	p2 := player.Player{Name: "young.thug"}
	p3 := player.Player{Name: "miles.davis"}
	db.Create(&p1)
	db.Create(&p2)
	db.Create(&p3)

	cm1 := chat.ChatMessage{
		PlayerID:   p2.ID,
		PlayerName: p2.Name,
		Streamgod:  false,
		Message:    "chillin",
	}
	tx := db.Create(&cm1)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}

	cm2 := chat.ChatMessage{
		PlayerID:   p3.ID,
		PlayerName: p3.Name,
		Streamgod:  false,
		Message:    "not chillin",
	}
	tx = db.Create(&cm2)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}

	parsedCmd := chat.ParsedCommand{
		Name:         "dropeffect",
		Username:     "beginbot",
		TargetUser:   "",
		TargetAmount: 2,
		Streamlord:   true,
		Streamgod:    true,
	}
	dropeffect(db, &parsedCmd, 2, chatResults, botbotResults, websocketResults)

	select {
	case <-time.After(time.Millisecond * 50):
		t.Error("Did not receive a message")
	case _ = <-websocketResults:
		if !sc2.IsAllowedToPlay2(db, p2.Name) {
			t.Errorf("We expected @young.thug to be allowed to play !hello")
		}
		if !sc.IsAllowedToPlay2(db, p3.Name) {
			t.Errorf("We expected @miles.davis to be allowed to play !damn")
		}
	}

}

// Why is this failing when running with all the tests
// !dropeffect damn 2
func TestDropeffectMultipleOfOneCommand(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	chatResults := make(chan string, 1000)
	botbotResults := make(chan string, 1000)
	websocketResults := make(chan string, 1000)
	defer close(websocketResults)
	defer close(chatResults)
	defer close(botbotResults)

	cmdName := "damn"
	sc := stream_command.CreateFromName(db, cmdName)

	p1 := player.Player{Name: "beginbot", Streamgod: true}
	p2 := player.Player{Name: "young.thug"}
	p3 := player.Player{Name: "gunna"}
	db.Create(&p1)
	db.Create(&p2)
	db.Create(&p3)

	cm1 := chat.ChatMessage{PlayerID: p2.ID}
	tx := db.Create(&cm1)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}
	cm2 := chat.ChatMessage{PlayerID: p3.ID}
	tx = db.Create(&cm2)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}
	fmt.Printf("sc = %+v\n", sc)

	parsedCmd := chat.ParsedCommand{
		Name:          "dropeffect",
		Username:      "beginbot",
		TargetCommand: "damn",
		TargetAmount:  2,
		Streamlord:    true,
		Streamgod:     true,
	}

	dropeffect(db, &parsedCmd, 2, chatResults, botbotResults, websocketResults)

	select {
	case <-time.After(time.Millisecond * 50):
		t.Error("Did not receive a message")
	case _ = <-websocketResults:

		if !sc.IsAllowedToPlay2(db, p3.Name) {
			t.Errorf("We expected @gunna to be allowed to play !damn")
		}
		if !sc.IsAllowedToPlay2(db, p2.Name) {
			t.Errorf("We expected @young.thug to be allowed to play !damn")
		}
	}
}

// var db *gorm.DB

// func TestMain(m *testing.M) {
// 	// This wouldn't be available unless I declared it outside the scope
// 	db = database.CreateDBConn("test_beginsounds3")
// 	m.Run()
// 	test_support.ClearDb(db)
// }
