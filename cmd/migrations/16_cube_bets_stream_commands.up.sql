CREATE TABLE public.cube_bet_stream_commands (
    id SERIAL,
    cube_bet_id int references cube_bets(id) NOT NULL,
    stream_command_id int references stream_commands(id) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT cube_bet_stream_commands_unique UNIQUE(cube_bet_id,stream_command_id),
    CONSTRAINT cube_bet_stream_commands_pkey PRIMARY KEY (id)
);
