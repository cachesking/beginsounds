CREATE TABLE public.cube_bets (
    id SERIAL,
    duration smallint NOT NULL,
    player_id int references players(id) NOT NULL,
    cube_game_id int references cube_games(id) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at timestamp with time zone,
    CONSTRAINT cube_bets_pkey PRIMARY KEY (id)
);
