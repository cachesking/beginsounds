package stream_jester

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"time"

	"github.com/google/uuid"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/the_mole"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

var Commands = []string{
	"!alerts",
	"!beginworld",
	"!h",
	"!highlight",
	"!j",
	"!k",
	"!l",
	"!pulse",
	"!pulseseal",
	"!reaction",
	"!rise",
	"!seal",
	"!spin",
	"!tallbegin",
	"!widebegin",
	"!zoomseal",
	"!bigbrainbegin",
	"!flipbegin",
	"!highlight",
	"!lbbegin",
	"!ltbegin",
	"!tlbegin",
	"!trbegin",
	"!blbegin",
	"!brbegin",
	"!normie",
	"!pulse",
	"!rise",
	"!rollbegin",
	"!rtbegin",
	"!spazzbegin",
	"!zoom2",
	"!zoom3",
	"!zoom4",
	"!zoombegin",
	"!zoomseal",
}

type StreamJester struct {
	ID        int
	PlayerID  int
	Secret    string
	ChaosMode bool
	Filename  string
}

func CurrentJester(db *gorm.DB) (*StreamJester, error) {
	var sj StreamJester

	tx := db.Table("stream_jesters").Where(`
		deleted_at IS NULL
	`).Order("created_at desc").First(&sj)

	return &sj, tx.Error
}

// NewSecret generates a new Stream Jester with a New
// 	secret, to be discovered
func NewSecret(db *gorm.DB) *StreamJester {
	fmt.Println("\tGenerating New Secret")
	commands := stream_command.AllNames(db)

	rand.Seed(time.Now().UnixNano())
	randomIndex := rand.Intn(len(commands))
	secret := commands[randomIndex]
	s := fmt.Sprintf("!%s", secret)

	uuidWithHyphen := uuid.New()
	f, err := os.Create(fmt.Sprintf("tmp/%s", uuidWithHyphen.String()))
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}
	f.WriteString(s)

	f2, err := os.Create(fmt.Sprintf("tmp/%s", "jester.txt"))
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}
	f2.WriteString(s)

	oldJester, _ := CurrentJester(db)
	db.Model(&oldJester).Update("deleted_at", time.Now())

	sj := StreamJester{
		Secret:   secret,
		Filename: uuidWithHyphen.String(),
	}
	tx := db.Create(&sj)
	if tx.Error != nil {
		fmt.Printf("tx.Error = %+v\n", tx.Error)
	}

	// create a new file in the tmp folder
	// filename := fmt.Sprintf("se")
	the_mole.LeakSecrets(db, uuidWithHyphen.String())

	return &sj
}

type jesterSyncFunc = func(*gorm.DB, *StreamJester)

func ChaosMode(db *gorm.DB, syncFunc jesterSyncFunc) (bool, error) {
	jester, err := CurrentJester(db)

	fmt.Printf("\tjester.Name = %+v\n", jester.Secret)
	fmt.Printf("\tjester.ChaosMode beforing fLipping = %+v\n", jester.ChaosMode)
	if err != nil {
		return false, err
	}

	tx := db.Model(&jester).Update("chaos_mode", !jester.ChaosMode)
	// syncFunc(db, jester)
	return jester.ChaosMode, tx.Error
}

func SyncWTF(db *gorm.DB, jester *StreamJester) {
	jesterName := player.FindByID(db, jester.PlayerID).Name
	go func() {
		data := map[string]interface{}{
			"chaos":  jester.ChaosMode,
			"jester": jesterName,
		}
		website_generator.JsonPage("wtf.json", data)
		jData, _ := json.Marshal(data)
		err := ioutil.WriteFile("build/wtf.json", jData, 0644)
		if err != nil {
			fmt.Printf("\nError Creating Build File: %s", err)
			return
		}
		website_generator.SyncJSONPage("wtf")
	}()
}
