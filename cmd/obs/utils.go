package obs

import (
	"math/rand"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

// These need to take in the OBSClient
type transformFuncCollection = func(
	*OBSClient,
	string,
	Command,
	chan<- string,
) (bool, error)

var funcOpts = []transformFunc{
	ChadSource,
	BigBrain,
	ZoomSpinSource,
	SlideInFromSide,
	floatSourceThroughScene,
}

// ===================================================

func RandomPos() (float64, float64) {
	x := float64(rand.Intn(1280))
	y := float64(rand.Intn(720))
	return x, y
}

func ProcessObsCommands(
	client *OBSClient,
	cmds <-chan Command,
) <-chan string {

	results := make(chan string)

	go func() {
		defer close(results)

		for cmd := range cmds {
			cmd.Func(client, cmd.Scene, cmd.Source, cmd.Settings)
		}
	}()

	return results
}

func findSourceAndScene(msg string) Command {
	parts := strings.Split(msg, " ")
	cmd := Command{Parts: parts}

	if len(parts) < 2 {
		return cmd
	}

	source := parts[1]
	cmd.Source = source

	switch cmd.Source {
	case "922":
		cmd.Scene = PrimaryScene
	case "twitchchat", "chat":
		cmd.Scene = PrimaryScene
		cmd.Source = "twitchchat"
	case "begin":
		cmd.Scene = PrimaryScene
		cmd.Source = "922"
	default:
		cmd.Scene = MemeScene
	}

	filters, ok := FilterMap[parts[0]]
	if ok {
		cmd.Filters = filters
	}
	return cmd
}

func randomTransformFunc() transformFunc {
	randInt := rand.Intn(len(funcOpts))
	return funcOpts[randInt]
}

// TODO: Move this
func findPlayer(db *gorm.DB, msg chat.ChatMessage) *player.Player {
	if msg.PlayerID != 0 {
		return player.FindByID(db, msg.PlayerID)
	}

	if msg.PlayerName != "" {
		return player.Find(db, msg.PlayerName)
	}

	return &player.Player{}
}

// The goal of this was pipe
// this is where we want pipe
func chatExploder(commands <-chan chat.ChatMessage) <-chan chat.ChatMessage {
	results := make(chan chat.ChatMessage, 10000)

	go func() {
		defer close(results)

		for msg := range commands {
			subCmds := strings.Split(msg.Message, "|")

			for _, cmd := range subCmds {

				c := chat.ChatMessage{
					PlayerName: msg.PlayerName,
					PlayerID:   msg.PlayerID,
					Streamlord: msg.Streamlord,
					Streamgod:  msg.Streamgod,
					StreetCred: msg.StreetCred,
					CoolPoints: msg.CoolPoints,
					Message:    strings.TrimSpace(cmd),
				}
				results <- c

			}
		}
	}()

	return results
}
