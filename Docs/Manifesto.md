# Manifesto

## FAQ

- Why can't I give out props to random people??
  - You are giving props to people you like,
    the purpose of props, is showing who you like
    in the system. Giving it to random people
    entirely defeats.
    You can create a circle of "Lovers", and
    undirected props go to those lovers.
    So it's not random. These are you're favorite
    viewers.
