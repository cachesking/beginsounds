CREATE TABLE public.memes (
    id SERIAL,
    name character varying(255) NOT NULL,
    position_type character varying(255) DEFAULT 'current' NOT NULL,

    x real DEFAULT 700.0,
    y real DEFAULT 500.0,
    rotation real DEFAULT 0.0,
    scale real DEFAULT 1.0,

    /* Original Size */
    media_type character varying(255) NOT NULL,

    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT memes_pkey PRIMARY KEY (id),
    CONSTRAINT memes_name_position_type_key UNIQUE(name,position_type)
);
