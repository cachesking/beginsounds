# Beginsounds

![](https://gitlab.com/beginbot/beginsounds/badges/master/pipeline.svg)

A Twitch Chat Bot Written in Go, to help facilitate various activities and wackiness on Beginbot's Stream.

!slide bitcoin
!reset bitcoin
!slide bitcoin

## Features:

- Play Soundeffects
- Change Begin's Colorscheme
- Submit soundeffects
- Pokemon Call Guessing Game
- Save All Chat (to sell to Zuckerberg)

## TODO

- Figure out how to silence, or the proper pattern for handling looking for
  records in the DB

## Requirements

### Packages

- libsdl2
- libsdl2_mixer
- gotest
- grc

## Running Migrations

```
createdb beginsounds

go build cmd/migrations/migrations.go

./migrations -db=beginsounds init

./migrations -db=beginsounds up

# Should see some output similar to:
> migrated from version 0 to 8
```

## Importing Data

```
go build data_migrations/json_to_postgres.go

./json_to_postgres -db=beginsounds
```
