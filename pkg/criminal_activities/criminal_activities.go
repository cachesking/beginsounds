package criminal_activities

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

// Potential Victim
type PotentialVictim struct {
	ID          int
	Name        string
	CommandName string
	CommandID   int
}

// 2 Paths:
//
// - Fast search to see if the user already owns, and exit early
//   better for thieves stealing their own sounds
//   every successful theft will be 3 queries: (look if you own, look up marks,
//   update)
//
// - iterate through the results and check if any player_ids match the current
// player
func AttemptToSteal(
	db *gorm.DB,
	msg chat.ChatMessage,
	parsedCmd *chat.ParsedCommand,
) string {
	var victims []PotentialVictim

	if parsedCmd.TargetUser != "" {
		// Gets a list of potentials "marks"
		/// These are join table records we are going to modify
		// We could also include the player name in the join right here
		db.Raw(`
			SELECT cp.player_id as id, p.name as name, sc.name as command_name, sc.id as command_id
				FROM commands_players cp
			INNER JOIN stream_commands sc
				ON cp.stream_command_id = sc.id
			INNER JOIN players p
				ON cp.player_id = p.id
			WHERE sc.name = ?
				AND p.name = ?
	`, parsedCmd.TargetCommand, parsedCmd.TargetUser).Scan(&victims)

		if len(victims) == 0 {
			return fmt.Sprintf("@%s can't steal !%s, @%s doesn't own it!",
				msg.PlayerName,
				parsedCmd.TargetCommand,
				parsedCmd.TargetUser)
		}

	} else {
		db.Raw(`
			SELECT cp.player_id as id, p.name as name, sc.name as command_name, sc.id as command_id
				FROM commands_players cp
			INNER JOIN stream_commands sc
				ON cp.stream_command_id = sc.id
			INNER JOIN players p
				ON cp.player_id = p.id
			WHERE sc.name = ?
	`, parsedCmd.TargetCommand).Scan(&victims)
	}

	if len(victims) == 0 {
		return fmt.Sprintf("@%s couldn't find anyone to steal !%s from", msg.PlayerName, parsedCmd.TargetCommand)
	}

	for _, v := range victims {
		if v.ID == msg.PlayerID {
			return fmt.Sprintf("@%s You can't steal what you already own !%s", msg.PlayerName, parsedCmd.TargetCommand)
		}
	}

	// We grab a random record for who we are going to steal from
	randomIndex := rand.Intn(len(victims))
	victim := victims[randomIndex]

	// We update the join table record, so the thief
	// is the new player
	db.Exec(`
		UPDATE commands_players
		SET player_id = ?
		WHERE player_id = ?
		AND stream_command_id = ?`,
		msg.PlayerID,
		victim.ID,
		victim.CommandID)

	return fmt.Sprintf("@%s stole !%s from @%s", msg.PlayerName, victim.CommandName, victim.Name)
}

func Serve(
	ctx context.Context,
	dbChan chat.DBChan,
) <-chan string {
	results := make(chan string)

	go func() {
		defer close(results)

		for msg := range dbChan.Messages {
			parsedCmd := msg.ParsedCmd

			select {
			case <-ctx.Done():
				return
			default:

				switch parsedCmd.Name {
				case "steal":
					rand.Seed(time.Now().UnixNano())

					success := []bool{true, false}
					randomIndex := rand.Intn(len(success))
					shouldSteal := success[randomIndex]

					if shouldSteal {
						res := AttemptToSteal(dbChan.DB, msg, &parsedCmd)
						results <- res
					} else {
						p := player.Player{ID: msg.PlayerID}
						dbChan.DB.Model(&p).Update("mana", 0)
						results <- fmt.Sprintf("@%s got caught stealing!!!! Lost all their mana", msg.PlayerName)
					}

					// So How do we know you can steal
					// How should we handle this here
					// WE should have the continue Msg Loop
					// But where will the logic exist
					// Time to Handle stealing
				}
			}
		}
	}()

	return results
}
