package streamgod_router

import (
	"fmt"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/soundboard"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gorm.io/gorm"
)

func PassTheJester(
	db *gorm.DB,
	msg chat.ChatMessage,
	parsedCmd *chat.ParsedCommand,
	chatResults chan<- string,
	aqs chan<- audio_request.AudioRequest,
) {

	jester, _ := stream_jester.CurrentJester(db)

	if jester.PlayerID == 0 {
		chatResults <- "No Current Jester"
		return
	}

	if !msg.Streamgod || jester.PlayerID == msg.PlayerID {
		chatResults <- "Not Allows to Pass Jester"
		return
	}

	p := potentialJester(db, msg)

	fmt.Printf("Old Jester: %v\n", jester)
	fmt.Printf("Chosen Jester!: %v\n", p.Name)

	tx := db.Model(&jester).Update("player_id", p.ID)
	if tx.Error != nil {
		fmt.Printf("Error Updating Jester: %+v\n", tx.Error)
	}
	aq, err := soundboard.CreateAudioRequest(db, "newjester", "beginbotbot")
	if err != nil {
		fmt.Printf("Error Creating Audio Request for New Jester: %+v\n", err)
		return
	}
	aqs <- *aq
	msg1 := strings.Join(stream_jester.Commands, " ")
	chatResults <- msg1
	res := fmt.Sprintf("GlitchLit GlitchLit New Jester! @%s GlitchLit GlitchLit", p.Name)
	chatResults <- res
}

func potentialJester(db *gorm.DB, msg chat.ChatMessage) *player.Player {
	parsedCmd := msg.ParsedCmd

	if parsedCmd.TargetUser != "" && parsedCmd.TargetUser != msg.PlayerName {
		return player.Find(db, parsedCmd.TargetUser)
	}

	players := chat.CurrentlyChatting(db, msg.PlayerID)
	p, _, err := RandomPlayer(db, players)
	if err != nil {
		fmt.Printf("Error Finding Random Player: %+v\n", err)
	}
	return p
}
