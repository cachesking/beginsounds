package obs

import (
	obsws "github.com/davidbegin/go-obs-websocket"
)

// ToggleFilter is meant to take a Scene, a filter
// and you can toggle a filter off easily!
func ToggleFilter(
	client *OBSClient,
	sourceName string,
	filterName string,
	toggle bool,
) {
	req := obsws.NewSetSourceFilterVisibilityRequest(sourceName, filterName, toggle)
	client.execOBSCommand(&req)
}

// ==========================================================================
// Functions that are filters are settings
// ==========================================================================

// ResetTransform sets all the settings for the "transform" filter
// 		back to defaults
func ResetTransform(client *OBSClient, sourceName string) {
	filterSettings := map[string]interface{}{
		"Filter.Transform.Camera.FieldOfView": 90.00,
		"Filter.Transform.Position.Y":         0.00,
		"Filter.Transform.Position.Z":         0.00,
		"Filter.Transform.Rotation.X":         0.00,
		"Filter.Transform.Rotation.Y":         0.00,
		"Filter.Transform.Rotation.Z":         0.00,
		"Filter.Transform.Scale.X":            90.00,
		"Filter.Transform.Scale.Y":            90.00,
		"Filter.Transform.Shear.X":            0.0,
		"Filter.Transform.Shear.Y":            0.0,
	}

	filterName := "transform"
	setSourceFilterSettings(client, sourceName, filterName, filterSettings)
}

func ResetColors(
	client *OBSClient,
	sourceName string,
) {
	settings := map[string]interface{}{
		"brightness": 0,
		"color":      Red,
		"contrast":   0,
		"gamma":      0,
		"hue_shift":  0,
		"opacity":    0,
		"saturation": 0,
	}

	filterName := "color_correction"
	setSourceFilterSettings(client, sourceName, filterName, settings)
}

func SetColorFade(
	client *OBSClient,
	sourceName string,
	filterName string,
	color float64,
) {
	settings := map[string]interface{}{
		"brightness":     0,
		"color":          color,
		"contrast":       0,
		"duration":       10000,
		"filter":         "color_correction",
		"gamma":          0,
		"hue_shift":      0,
		"opacity":        32,
		"saturation":     0,
		"single_setting": false,
		"start_trigger":  5,
		"stop_trigger":   4,
		"value_type":     0,
	}
	setSourceFilterSettings(client, sourceName, filterName, settings)
}

func UpdateNormieTransform(
	client *OBSClient,
	sceneName string,
	sourceName string,
) {
	filterSettings := map[string]interface{}{
		"Filter.Transform.Camera.FieldOfView": 90,
		"Filter.Transform.Position.X":         0,
		"Filter.Transform.Position.Y":         0,
		"Filter.Transform.Position.Z":         0,
		"Filter.Transform.Rotation.X":         0,
		"Filter.Transform.Rotation.Y":         0,
		"Filter.Transform.Rotation.Z":         0,
		"Filter.Transform.Scale.X":            100,
		"Filter.Transform.Scale.Y":            100,
		"Filter.Transform.Shear.X":            0,
		"Filter.Transform.Shear.Y":            0,
		"duration":                            3000,
		"filter":                              "transform",
		"setting_float":                       0,
		"single_setting":                      false,
		"start_trigger":                       0,
		"value_type":                          0,
	}
	filterName := "normie_transform"
	setSourceFilterSettings(client, sourceName, filterName, filterSettings)
}

// TODO: This needs to take in Settings
// Turn it on
func OutlineColor(
	client *OBSClient,
	color float64,
	sourceName string,
) {
	filterName := "outline"
	filterSettings := map[string]interface{}{"Filter.SDFEffects.Outline.Color": color}
	req := obsws.NewSetSourceFilterSettingsRequest(sourceName, filterName, filterSettings)
	client.execOBSCommand(&req)
}
