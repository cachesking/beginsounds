# Uploading JSON

A quick easy hack off an API, is uploading JSON to object storage and letting users just query that.

## How to upload JSON in Beginsounds

```
go func() {
	JsonPage("parties.json", artmememes)
	website_generator.SyncPage("artmemes")
}()
```
