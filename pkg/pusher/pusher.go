package pusher

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/linode/linodego"
	"github.com/linode/terraform-provider-linode/linode"
	"golang.org/x/oauth2"
	"gorm.io/gorm"
)

type AccessKey struct {
	ID          uint
	Name        string
	AccessKey   string
	SecretKey   string
	BucketName  string
	Permissions string
}

// Need a created_at
type ScavengerSecret struct {
	Code        string
	AccessKeyID uint
}

func GetLinodeClient() (*linodego.Client, error) {
	token := os.Getenv("LINODE_TOKEN")
	if token == "" {
		return nil, fmt.Errorf("LINODE_TOKEN must be set for acceptance tests")
	}

	config := &linode.Config{AccessToken: token, APIVersion: "v4beta"}
	client := config.Client()
	return &client, nil
}

func CreateAndDeleteKeys(db *gorm.DB, permissions string) (*AccessKey, error) {
	region := "us-east-1"
	bucketName := "scavenger-bucket"

	apiKey, ok := os.LookupEnv("LINODE_TOKEN")
	if !ok {
		log.Fatal("Could not find LINODE_TOKEN, please assert it is set.")
	}
	tokenSource := oauth2.StaticTokenSource(&oauth2.Token{AccessToken: apiKey})

	oauth2Client := &http.Client{
		Transport: &oauth2.Transport{
			Source: tokenSource,
		},
	}

	linodeClient := linodego.NewClient(oauth2Client)

	// Delete the Old Keys
	var listOpts *linodego.ListOptions
	keys, _ := linodeClient.ListObjectStorageKeys(context.Background(), listOpts)
	for _, key := range keys {
		if key.Label == bucketName {
			linodeClient.GetObjectStorageKey(context.Background(), key.ID)
			linodeClient.DeleteObjectStorageKey(context.Background(), key.ID)
		}
	}

	// Create New Access Key
	bucketAccessOpts := linodego.ObjectStorageKeyBucketAccess{
		Cluster:     region,
		BucketName:  bucketName,
		Permissions: permissions,
	}
	opts := []linodego.ObjectStorageKeyBucketAccess{bucketAccessOpts}
	createOpts := linodego.ObjectStorageKeyCreateOptions{
		Label:        "scavenger-bucket",
		BucketAccess: &opts,
	}
	res, err := linodeClient.CreateObjectStorageKey(context.Background(), createOpts)
	if err != nil {
		fmt.Printf("Error creating Keys: %+v\n", err)
	}

	// We need to also create a secret of some some sort
	// how should that look
	// could be a sound a code
	// That maps

	// Should we assoicate this wtih the access key

	ac := AccessKey{
		Name:        bucketName,
		AccessKey:   res.AccessKey,
		SecretKey:   res.SecretKey,
		BucketName:  bucketName,
		Permissions: permissions,
	}

	// Any other fields we need to save
	// Someone sends a command
	// We do a query with
	// !secret38927398472k
	// Give me the current access keys, secrets
	// And check if the user matches
	secret := ScavengerSecret{
		Code:        "cool_random_code_here",
		AccessKeyID: ac.ID,
	}
	fmt.Printf("secret = %+v\n", secret)

	tx := db.Create(&ac)

	if tx.Error != nil {
		fmt.Printf("Error Creating Access Key: %+v\n", tx.Error)
	}

	// We need to choose a random sound
	// We need to create a file named after that sound, and put it in the bucket
	// (Note we )
	return &ac, tx.Error
}

func NewPut() {
	ctx := context.Background()

	// I could pass in configs into here
	// This ain't working
	sess := session.Must(
		session.NewSession(
			&aws.Config{
				Region:   aws.String("us-east"),
				Endpoint: aws.String("https://us-east-1.linodeobjects.com"),
			},
		))

	bucket := "scavenger-bucket"
	region, err := s3manager.GetBucketRegion(ctx, sess, bucket, "us-east-2")
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok && aerr.Code() == "NotFound" {
			fmt.Fprintf(os.Stderr, "unable to find bucket %s's region not found\n", bucket)
		}
	}
	fmt.Printf("Bucket %s is in %s region\n", bucket, region)
}

func PutObj() {
	// _, err := GetLinodeClient()
	// if err != nil {
	// 	fmt.Printf("Error GetLinodeClient: %+v\n", err)
	// }
	// The problem is this does exist for that client
	// var listOpts *linodego.ListOptions
	// client, _ := GetLinodeClient()
	// objectStorageBuckets, err := client.ListObjectStorageBuckets(context.Background(), listOpts)
	// fmt.Printf("objectStorageBuckets = %+v\n", objectStorageBuckets)
	// if err != nil {
	// 	fmt.Printf("err = %+v\n", err)
	// }
	// The session the S3 Downloader will use
	// sess := session.Must(session.NewSession())
	// sess := s3.New(session.New(&aws.Config{
	// 	Region: aws.String("us-east-1"),
	// 	// Credentials: credentials.NewStaticCredentials(accessKey, secretKey, ""),
	// 	Endpoint: aws.String("https://us-east-1.linodeobjects.com"),
	// }))
	// Credentials: credentials.NewStaticCredentials(accessKey, secretKey, ""),
	sess := session.New(&aws.Config{
		Region:   aws.String("us-east-1"),
		Endpoint: aws.String("https://us-east-1.linodeobjects.com"),
	})

	// Create a downloader with the session and default options
	// filename := "test.txt"
	myBucket := "scavenger-bucket"
	myString := "test.txt"
	// Create an uploader with the session and default options
	fmt.Println("CREATING NEW UPLOADER")
	uploader := s3manager.NewUploader(sess)

	// f, err := os.Open(filename)
	// if err != nil {
	// 	fmt.Errorf("failed to open file %q, %v", filename, err)
	// }

	f := strings.NewReader("nice")

	// Upload the file to S3.
	result, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(myBucket),
		Key:    aws.String(myString),
		Body:   f,
	})
	if err != nil {
		fmt.Errorf("failed to upload file, %v", err)
	}
	fmt.Printf("result = %+v\n", *result)
	// fmt.Printf("result = %+v\n", result.Location)
	// fmt.Printf("file uploaded to, %s\n", &aws.StringValue(result.Location))

	// downloader := s3manager.NewDownloader(sess)

	// // Create a file to write the S3 Object contents to.
	// f, err := os.Create(filename)
	// if err != nil {
	// 	fmt.Errorf("failed to create file %q, %v", filename, err)
	// }

	// // Write the contents of S3 Object to the file
	// n, err := downloader.Download(f, &s3.GetObjectInput{
	// 	Bucket: aws.String(myBucket),
	// 	Key:    aws.String(myString),
	// })
	// if err != nil {
	// 	fmt.Errorf("failed to download file, %v", err)
	// }
	// fmt.Printf("file downloaded, %d bytes\n", n)
	// We can generate specific keys for these values, a new set of keys
	// How are we going to pull them in.
	// Once deployed, most likely will be through environment variables

	// out, err := conn.GetObject(
	// 	&s3.GetObjectInput{
	// 		Bucket:  &bucket,
	// 		Key:     &key,
	// 		IfMatch: &etag,
	// 	})

	apiKey, ok := os.LookupEnv("LINODE_TOKEN")
	if !ok {
		log.Fatal("Could not find LINODE_TOKEN, please assert it is set.")
	}
	tokenSource := oauth2.StaticTokenSource(&oauth2.Token{AccessToken: apiKey})

	oauth2Client := &http.Client{
		Transport: &oauth2.Transport{
			Source: tokenSource,
		},
	}

	linodeClient := linodego.NewClient(oauth2Client)
	linodeClient.SetDebug(true)
	// linodeClient.CreateObjectStorageKey()
	conn := s3.New(session.New(&aws.Config{
		Region: aws.String("us-east-1"),
		// Credentials: credentials.NewStaticCredentials(accessKey, secretKey, ""),
		Endpoint: aws.String("https://us-east-1.linodeobjects.com"),
	}))
	fmt.Printf("conn = %+v\n", conn)
	// var listOpts *linodego.ListOptions
	// keys, _ := linodeClient.ListObjectStorageKeys(context.Background(), listOpts)

	// linodeClient.CreateObjectStorageKey()
	// fmt.Printf("linodeClient = %+v\n", linodeClient)

	// res, _ := linodeClient.ListObjectStorageBuckets(context.Background(), listOpts)
	// bucketAccessOpts := linodego.ObjectStorageKeyBucketAccess{Cluster: "us-east-1", BucketName: "beginworld", Permissions: "read_only"}
	// opts := []linodego.ObjectStorageKeyBucketAccess{bucketAccessOpts}

	// listOpts2 := linodego.ObjectStorageKeyCreateOptions{Label: "twitch_chatters", BucketAccess: &opts}
	// res1, _ := linodeClient.CreateObjectStorageKey(context.Background(), listOpts2)
	// fmt.Printf("res = %+v\n", res1)

	// linodeClient.GetObjectStorageKey()
	// var listOpts *linodego.ListOptions
	// keys, _ := linodeClient.ListObjectStorageKeys(context.Background(), listOpts)
	// fmt.Printf("keys = %+v\n", keys)
	// for _, key := range keys { if key.Label == "beginworld-1" { // linodeClient.GetObjectStorageKey(context.Background(), key.ID) linodeClient.DeleteObjectStorageKey(context.Background(), key.ID)
	// 	}
	// }

	// We need to save the info
	// can we delete the Key
	// linodeClient.DeleteObjectStorageKey(res1.ID)
}
