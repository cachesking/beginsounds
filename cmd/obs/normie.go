package obs

// ReturnToNormie This is meant to return all of BeginWorld
// 	back to a normal state
func ReturnToNormie(client *OBSClient) {
	standardSources := []string{
		"Screen",
		"Chat",
		"922",
		"keyboard",
	}
	transformSources := []string{
		"beginworld",
		"sealspin",
		"puppy",
	}
	scenes := []string{
		PrimaryScene,
		"TopRight",
		"TopLeft",
		"BottomLeft",
		// "Giger",
		// "hottub",
	}
	toggleSources := []string{
		"922-left-bottom",
		"922-left-top",
		"922-right-top",
		"primeagen",
		"melkman",
		"teejpopup",
		"melkeyussr",
		"beginboy",
		"artmattcardflat",
		"artmattfloppycard",
		"JesterCode",
	}

	for _, source := range standardSources {
		ToggleFilter(client, source, "outline", false)
		ToggleFilter(client, source, "transform", false)
	}

	for _, source := range transformSources {
		ToggleFilter(client, source, "outline", false)
	}

	for _, scene := range scenes {
		for _, source := range transformSources {
			ToggleSource(client, scene, source, false)
		}
		ToggleSource(client, PrimaryScene, "i_need_attention", false)
	}
	ToggleFilter(client, "922", "zoom", false)
	ToggleFilter(client, "Alerts", "transform", false)
	ToggleFilter(client, "Alerts", "outline", false)

	for _, source := range toggleSources {
		ToggleSource(client, MemeScene, source, false)
	}
}
