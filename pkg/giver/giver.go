package giver

import (
	"errors"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/permissions"
	"gorm.io/gorm"
)

func Giver(
	db *gorm.DB,
	admin chan<- string,
	beginbot chan<- string,
	msg chat.ChatMessage,
) (string, error) {

	fmt.Printf("Is Allowed: PlayerID: %d CommandID: %d\n", msg.PlayerID, msg.ParsedCmd.TargetCommandID)

	playerOwnsGift := permissions.IsAllowed(db, msg.PlayerID, msg.ParsedCmd.TargetCommandID)
	if !playerOwnsGift {
		return "", errors.New(
			fmt.Sprintf("@%s does NOT own !%s", msg.PlayerName, msg.ParsedCmd.TargetCommand),
		)
	}

	err := permissions.Allow(db, msg.ParsedCmd.TargetUserID, msg.ParsedCmd.TargetCommandID)
	if err != nil {
		return fmt.Sprintf(
			"Erroring @%s givin %s !%s",
			msg.PlayerName,
			msg.ParsedCmd.TargetUser,
			msg.ParsedCmd.TargetCommand,
		), errors.New(fmt.Sprintf("@%s Already owns !%s", msg.ParsedCmd.TargetUser, msg.ParsedCmd.TargetCommand))
	}

	err = permissions.Unallow(db, msg.PlayerID, msg.ParsedCmd.TargetCommandID)

	return fmt.Sprintf(
		"@%s gave %s !%s",
		msg.PlayerName,
		msg.ParsedCmd.TargetUser,
		msg.ParsedCmd.TargetCommand,
	), err
}
