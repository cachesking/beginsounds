package sound_store

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestBuy(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	playerName := "beginbot"
	p := player.CreatePlayerFromName(db, playerName)
	command := "hello"
	c := stream_command.CreateFromName(db, command)

	if player.AllowedToPlay(db, p.ID, c.ID) {
		t.Errorf("User Should not be allowed to play")
	}

	// result := Buy(db, playerName, "fake_not_real_command")

	// if result != fmt.Sprintf("@%s can't buy !%s it doesn't exist!", playerName, "fake_not_real_command") {
	// 	t.Errorf("User Should not be able buy fake commands")
	// }

	// result = Buy(db, playerName, command)

	// if result != fmt.Sprintf("@%s can't afford !%s %d/%d", playerName, command, 0, 1) {
	// 	t.Errorf("User Should NOT have enough money to buy !%s", command)
	// }

	// db.Model(&p).Update("cool_points", 3)
	// result = Buy(db, playerName, command)
	// if result == fmt.Sprintf("@%s can't afford !%s %d/%d", playerName, command, 0, 1) {
	// 	t.Errorf("User Should have enough money to buy !%s", command)
	// }

	// res2 := Buy(db, playerName, command)
	// if res2 != fmt.Sprintf("@%s already owns !%s", playerName, command) {
	// 	t.Errorf("You should get a message saying you already own the sound: %s", res2)
	// }

	// // we also need to make sure the cost moved right
	// // This is too reload
	// p = player.Find(db, playerName)
	// c = stream_command.Find(db, command)

	// if p.CoolPoints != 2 {
	// 	t.Errorf("We didn't charge for the sound %d", p.CoolPoints)
	// }

	// if c.Cost != 2 {
	// 	t.Errorf("The Sound didn't increase in value like it should: %d", c.Cost)
	// }
}
