package economy_router

import (
	"context"
	"fmt"
	"log"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/pack"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

// PackRouter routes commands related to the purchasing
//  and distribution of packs
func PackRouter(
	ctx context.Context,
	db *gorm.DB,
	messages <-chan chat.ChatMessage,
) (<-chan string, <-chan string) {

	botbotResponses := make(chan string)
	chatResponses := make(chan string)

	go func() {
		for msg := range messages {
			parsedCmd := msg.ParsedCmd
			parts := msg.Parts

			switch parsedCmd.Name {
			case "listpacks":
				names, err := pack.AllNames(db)
				if err == nil {
					site := "http://www.beginworld.exchange/packs.html"
					m := fmt.Sprintf("Packs: %s | %s", strings.Join(names, ", "), site)
					chatResponses <- m
					pack.GeneratePacksPage(db)
					website_generator.SyncRootPage("packs")
				}
			case "createpack":
				p, err := pack.CreatePackFromParts(db, parts[1:])
				// Should we pass this to the pack builder
				// botbotResponses <- "New Pack party"
				if err == nil {
					botbotResponses <- fmt.Sprintf("New Pack: %s", p.Name)
				}
			case "addtopack":
				packName := parts[1]
				cps, _ := pack.AddSoundsToPacks(db, packName, parts[2:])
				for _, command := range cps {
					botbotResponses <- fmt.Sprintf(
						"New Command: %s Added to Pack: %s",
						command,
						packName,
					)
				}
				// s = "New Sound in \"party\": !damn"
			case "removefrompack":
				// case "approvepack":
				// case "denypack":

			case "droppack":
				potentialPack := parts[1]
				p, _ := pack.FindByName(db, potentialPack)
				// playa := player.FindByName(db, parsedCmd.TargetUser)
				p.GiveToPlayer(db, parsedCmd.TargetUser)
				chatResponses <- fmt.Sprintf(
					"@%s got pack: %s",
					parsedCmd.TargetUser,
					potentialPack,
				)

			case "buypack":
				if parsedCmd.TargetPack != "" {
					tp, err := pack.FindByName(db, parsedCmd.TargetPack)
					if err != nil {
						log.Print(err)
					}

					pp := pack.PacksPlayers{
						PlayerID: msg.PlayerID,
						PackID:   tp.ID,
					}

					tx := db.Create(&pp)
					if tx.Error != nil {
						fmt.Printf("Error giving user pack: %+v\n", tx.Error)
					}

					msg := fmt.Sprintf("@%s bought Pack: %s", msg.PlayerName, tp.Name)
					chatResponses <- msg
				}
			}
		}
	}()

	return botbotResponses, chatResponses
}
