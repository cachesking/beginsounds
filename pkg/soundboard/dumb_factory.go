package soundboard

import (
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

// TODO: Update this to simpler
// and not look up the samples
func CreateAdminAudioRequest(
	db *gorm.DB,
	potentialSample string,
) audio_request.AudioRequest {
	samples := FetchSamples()

	username := "beginbotbot"
	p := player.Find(db, username)

	for _, sample := range samples {
		ls := strings.ToLower(potentialSample)

		if ls[1:] == sample.Name || ls == sample.Name {
			sample.Username = username

			a := audio_request.AudioRequest{
				PlayerID: p.ID,
				Name:     sample.Name,
				Filename: sample.Filename,
				Notify:   false,
				Path:     sample.Path}

			audio_request.Save(db, &a)
			return a

		}
	}

	return audio_request.AudioRequest{}
}
