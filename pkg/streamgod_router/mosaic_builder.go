package streamgod_router

import (
	"fmt"
	"os/exec"
	"path/filepath"

	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gorm.io/gorm"
)

func buildMosaic(db *gorm.DB, parts []string) {
	fmt.Println("Hitting Mosaic")
	if len(parts) < 2 {
		return
	}

	image2Mosic := parts[1]

	var media media_request.MediaRequest
	tx := db.Model(&media_request.MediaRequest{}).
		Where("name = ?", image2Mosic).
		Order("created_at DESC").
		Scan(&media)

	if tx.Error != nil {
		return
	}
	ext := filepath.Ext(media.Filename)
	args := []string{
		fmt.Sprintf("-target=/home/begin/stream/Stream/ViewerImages/%s%s", media.Name, ext),
		"-tiles=/home/begin/stream/Stream/ViewerImages/",
		fmt.Sprintf("-out=Art/%s%s", media.Name, ext),
	}
	mosaicCmd := exec.Command(
		"/home/begin/code/opensource/GCSolutions/may15/normal/david-le-corfec/mosy/mosy",
		args...,
	)

	fmt.Printf("args = %+v\n", args)

	err := mosaicCmd.Run()
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}
}
