package router

import (
	"context"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/chat_parser"
	"gitlab.com/beginbot/beginsounds/pkg/config"
	"gitlab.com/beginbot/beginsounds/pkg/irc"
	"gitlab.com/beginbot/beginsounds/pkg/parser"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

func FilterChatMsgs(
	ctx context.Context,
	db *gorm.DB,
	c *config.Config,
	messages <-chan string,
) <-chan chat.ChatMessage {

	UsersWeDoNotLike := []string{"nightbot"}

	userMsgs := make(chan chat.ChatMessage, 1000)

	go func() {
		defer close(userMsgs)

		for msg := range messages {
		outside:

			select {
			case <-ctx.Done():
				return
			default:

				if parser.IsPrivmsg(msg) {
					user, m := parser.ParsePrivmsg(msg)
					p := player.FindOrCreate(db, user)

					chatMessage, _ := chat_parser.Parse(db, p, m)

					for _, u := range UsersWeDoNotLike {
						if u == user {
							break outside
						}
					}

					userMsgs <- chatMessage

				} else if parser.IsPing(msg) {
					irc.Pong(c)
				}

			}
		}
	}()

	return userMsgs
}
