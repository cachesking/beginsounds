CREATE TABLE public.stream_commands(
    id SERIAL,
    name character varying(255) NOT NULL UNIQUE,
    cost int DEFAULT 0 NOT NULL,
    theme bool DEFAULT false NOT NULL,
    filename character varying(255) NOT NULL UNIQUE,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    owner_id int references players(id),
    CONSTRAINT stream_commands_pkey PRIMARY KEY (id)
);
