package main

import (
	"net/http"

	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
)

// HelpHandler builds the remote site and servers the local site
func HelpHandler(w http.ResponseWriter, r *http.Request) {
	website_generator.CreateHelpPage()
	website_generator.SyncPage("help")
	page := website_generator.HelpPage{
		Domain: "http://localhost:1992",
	}
	helpTmpl.Execute(w, page)
}
