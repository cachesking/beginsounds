package soundboard

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/permissions"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestCreateAudioRequest(t *testing.T) {
	// Sample Doesn't exist
	// User is not allowed
	// User is allowed

	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	// username := "beginbot"
	// sc := stream_command.CreateFromName(db, "cool")
	// fmt.Printf("sc = %+v\n", sc)

	// chat.ChatMessage{Message: "!fake_sample"}
	// aq, _ := CreateAudioRequest(db, "fake_sample", username)
	// fmt.Printf("aq = %+v\n", aq)
	// if aq.Name != "" {
	// 	t.Errorf("We should not have gotten an audio request back")
	// }

	// TODO: readd once we readd permissions
	// err, aq = CreateAudioRequest(db, "cool", username)
	// if err != nil && aq.Name != "cool" {
	// 	t.Errorf("We should have not gotten an error")
	// }
}

func TestFindSample(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	sc := stream_command.CreateFromName(db, "cool")
	fmt.Printf("sc = %+v\n", sc)

	res1 := Find(db, "invalid_sample")
	if res1.Name != "" {
		t.Errorf("We found an invalid sample: %s", res1.Name)
	}
	res2 := Find(db, "cool")
	if res2.Name != "cool" {
		t.Errorf("We should have found cool: %s", res2.Name)
	}

}

func TestAudioRequest(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	player.CreatePlayerFromName(db, "beginbot")
	stream_command.CreateFromName(db, "damn")

	permissions.UserAllowedToPlay(db, "beginbot", "damn")
	aq, _ := CreateAudioRequest(db, "!damn", "beginbot")
	fmt.Printf("sample = %+v\n", aq)

	// count := audio_request.Count(db)
	// if count != 1 {
	// 	t.Errorf("We did not save a theme: %d", count)
	// }
}
