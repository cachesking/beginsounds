package website_generator

import (
	"fmt"
	"os"
	"os/exec"
	"strings"
)

func SyncRootPage(page string) {
	put(
		fmt.Sprintf("/home/begin/code/beginsounds/build/%s.html", page),
		fmt.Sprintf("%s.html", page),
	)
}

func SyncPlayerCSS(player string) {
	put(
		fmt.Sprintf("/home/begin/code/beginsounds/assets/static/%s.css", player),
		fmt.Sprintf("assets/%s.css", player),
	)
}

// these sync functions can be separate
func SyncCommandSite(command string) {
	put(
		fmt.Sprintf("/home/begin/code/beginsounds/build/commands/%s.html", command),
		fmt.Sprintf("commands/%s.html", command),
	)
}

func SyncCommandsJson(command string) {
	put(
		fmt.Sprintf("/home/begin/code/beginsounds/build/commands.json"),
		fmt.Sprintf("commands.json"),
	)
}

func SyncJSONPage(page string) {
	put(
		fmt.Sprintf("/home/begin/code/beginsounds/build/%s.json", page),
		fmt.Sprintf("%s.json", page),
	)
}

func SyncPage(page string) {
	put(
		fmt.Sprintf("/home/begin/code/beginsounds/build/%s.html", page),
		fmt.Sprintf("%s.html", page),
	)
}

func SyncPartiesSite() {
	put(
		fmt.Sprintf("/home/begin/code/beginsounds/build/parties.html"),
		fmt.Sprintf("parties.html"),
	)
}

func SyncMemesPage() {
	put(
		fmt.Sprintf("/home/begin/code/beginsounds/build/memes.html"),
		fmt.Sprintf("memes.html"),
	)

	put(
		fmt.Sprintf("/home/begin/code/beginsounds/build/memes.json"),
		fmt.Sprintf("memes.json"),
	)
}

func SyncUserSite(user string) {
	put(
		fmt.Sprintf("/home/begin/code/beginsounds/build/users/%s.html", user),
		fmt.Sprintf("users/%s.html", user),
	)
}

func SyncSite() {
	sync(
		"/home/begin/code/beginsounds/build/",
		"/",
	)
}

func sync(from string, to string) {
	cmd := "s3cmd"
	args := []string{
		"--no-mime-magic",
		"--acl-public",
		"--delete-after",
		"sync",
		from,
		fmt.Sprintf("s3://beginworld%s", to),
	}
	if err := exec.Command(cmd, args...).Run(); err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}

func put(from string, to string) {
	cmd := "s3cmd"
	args := []string{"--no-mime-magic",
		"--acl-public",
		"--delete-after",
		"put",
		from,
		fmt.Sprintf("s3://beginworld/%s", to),
	}

	fullCmd := fmt.Sprintf("%s %s", cmd, strings.Join(args, " "))

	err := exec.Command(cmd, args...).Run()
	if err != nil {
		fmt.Printf("\nError Syncing Meme HTML: %+v\n%s\n", err, fullCmd)
	}
}
