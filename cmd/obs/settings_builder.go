package obs

import (
	"fmt"
	"math"
	"strconv"

	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

// This is where we handle any massaging of settings
// from user inputs, until it hits OBS
func settingsBuilder(
	client *OBSClient,
	cmd string,
	obsCmd Command,
) (
	map[string]interface{},
	*memes.Meme,
	*memes.Meme,
) {

	// We Find
	defaultMeme, _ := memes.FindDefault(client.DB, obsCmd.Source)
	currentMeme, _ := memes.FindCurrent(client.DB, obsCmd.Source)
	fmt.Printf("\n\tCurrent Meme: %+v\n\n", currentMeme)
	settings := map[string]interface{}{
		"x":        defaultMeme.X,
		"start_x":  defaultMeme.X,
		"end_x":    defaultMeme.X,
		"y":        defaultMeme.Y,
		"start_y":  defaultMeme.Y,
		"end_y":    defaultMeme.Y,
		"rotation": defaultMeme.Rotation,
		"scale":    defaultMeme.Scale,
	}

	switch cmd {
	case "!spin":
		settings["delay"] = 5
	case "!scale":
		settings["scale"] = 1.0

		if len(obsCmd.Parts) > 2 {
			scale, err := strconv.ParseFloat(obsCmd.Parts[2], 64)

			if err != nil {
				return settings, defaultMeme, currentMeme
			}

			if math.IsNaN(scale) || math.IsInf(scale, 0) {
				return settings, defaultMeme, currentMeme
			}

			settings["scale"] = scale
		}

		fmt.Printf("Setting settings For Scale: %+v\n", settings)
		return settings, defaultMeme, currentMeme
	case "!zoom":
		level := 200.0
		if len(obsCmd.Parts) > 2 {
			level, _ = strconv.ParseFloat(obsCmd.Parts[2], 64)
		}

		settings["level"] = level
	case "!slide":

		if currentMeme.ID == 0 {
			if defaultMeme.X < 400.0 {
				settings["start_x"] = 1200.0
			} else {
				settings["start_x"] = 0.0
			}
		} else {
			settings["start_x"] = currentMeme.X
		}

	case "!slideout":
		settings["end_x"] = -400.0
	case "!rotate":
		if obsCmd.Scene != "" && obsCmd.Source != "" {
			rotation := 0.0

			if len(obsCmd.Parts) > 2 {
				rotation, _ = strconv.ParseFloat(obsCmd.Parts[2], 64)
			}
			settings["rotation"] = rotation
		}

	case "!color":
		if len(obsCmd.Parts) > 2 {
			colorName := obsCmd.Parts[2]
			// We look for the color by name
			rawColor, ok := ColorCodes[colorName]
			if ok {
				color, _ := strconv.ParseFloat(rawColor, 64)

				fmt.Printf("colorName, = %v color %v\n", colorName, color)
				settings["color"] = color
			}

		}
	case "!rise":
		settings["start_y"] = 1000.0
		settings["modifier"] = modifierExtractor(obsCmd.Parts, 0.8)
		settings["x"] = currentMeme.X
	case "!unveil":
		settings["start_y"] = 400.0
	case "!fall":
		settings["modifier"] = modifierExtractor(obsCmd.Parts, 0.8)
		settings["start_y"] = currentMeme.Y
		settings["x"] = currentMeme.X
	case "!show":
		settings["toggle"] = true
	case "!hide":
		settings["toggle"] = false
	case "!moveright":
		modifier := modifierExtractor(obsCmd.Parts, 100.0)
		newX := currentMeme.X + modifier

		settings = map[string]interface{}{
			"x":        newX,
			"start_x":  currentMeme.X,
			"end_x":    newX,
			"y":        currentMeme.Y,
			"start_y":  currentMeme.Y,
			"end_y":    currentMeme.Y,
			"rotation": currentMeme.Rotation,
			"scale":    currentMeme.Scale,
		}

	case "!moveleft":
		modifier := modifierExtractor(obsCmd.Parts, 100.0)
		newX := currentMeme.X - modifier

		settings = map[string]interface{}{
			"x":        newX,
			"start_x":  currentMeme.X,
			"end_x":    newX,
			"y":        currentMeme.Y,
			"start_y":  currentMeme.Y,
			"end_y":    currentMeme.Y,
			"rotation": currentMeme.Rotation,
			"scale":    currentMeme.Scale,
		}

	case "!movedown":
		modifier := modifierExtractor(obsCmd.Parts, 100.0)
		newY := currentMeme.Y + modifier

		settings = map[string]interface{}{
			"x":        currentMeme.X,
			"start_x":  currentMeme.X,
			"end_x":    currentMeme.X,
			"y":        newY,
			"start_y":  currentMeme.Y,
			"end_y":    newY,
			"rotation": currentMeme.Rotation,
			"scale":    currentMeme.Scale,
		}

	case "!moveup":
		modifier := modifierExtractor(obsCmd.Parts, 100.0)
		newY := currentMeme.Y - modifier

		settings = map[string]interface{}{
			"x":        currentMeme.X,
			"start_x":  currentMeme.X,
			"end_x":    currentMeme.X,
			"y":        newY,
			"start_y":  currentMeme.Y,
			"end_y":    newY,
			"rotation": currentMeme.Rotation,
			"scale":    currentMeme.Scale,
		}

	case "!move":
		if len(obsCmd.Parts) > 3 {
			rawX := obsCmd.Parts[2]
			rawY := obsCmd.Parts[3]
			x, _ := strconv.ParseFloat(rawX, 64)
			y, _ := strconv.ParseFloat(rawY, 64)
			settings["x"] = x
			settings["y"] = y
		}

	case "!flash", "!default":
		// This doesn't need to anything
		// the top of the switch statement
		// setting of default Meme does it all
		fmt.Println("Setting settings for flash: ", obsCmd.Source)
	}
	return settings, defaultMeme, currentMeme
}

func modifierExtractor(parts []string, modifier float64) float64 {
	if len(parts) > 2 {
		m, err := strconv.ParseFloat(parts[2], 64)
		if err != nil {
			fmt.Printf("Moveup err = %+v\n", err)
		} else {
			modifier = m
		}
	}

	return modifier
}
