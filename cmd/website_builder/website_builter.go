package main

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"os"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/stream_command"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
)

type PlayerPage struct {
	Name         string
	CoolPoints   uint
	StreetCred   uint
	Lovers       []website_generator.LoverResult
	CommandCount int64
	Commands     []website_generator.CommandResult
	Domain       string
	Extension    string
}

func main() {
	db := database.CreateDBConn("beginsounds4")

	dat, _ := ioutil.ReadFile("media2.txt")
	res := bytes.Split(dat, []byte{'\n'})

	allFiles := stream_command.AllFiles(db)
	for _, file := range allFiles {
		match := false

		for _, r := range res {
			if file == string(r) {
				match = true
			}
		}

		if !match {
			fmt.Printf("NO MATCH FOR: %+v\n", file)
		}
	}
	// fmt.Printf("dat = %+v\n", dat)

	return
	user_tmpl := template.Must(template.ParseFiles("templates/user.html", "templates/sub_templates.tmpl"))
	users := player.AllNames(db)
	for _, user := range users {
		p := player.Find(db, user)

		lovers := website_generator.LoverNames(db, p.ID)
		fmt.Printf("\tlovers = %+v\n", lovers)
		cmds := website_generator.CommandsAndCosts(db, p.ID)
		var cmdCount int64
		res := db.Raw(`
			SELECT count(*) FROM commands_players c
			WHERE c.player_id = ?
	`, p.ID).Count(&cmdCount)
		if res.Error != nil {
			fmt.Printf("res.Error = %+v\n", res.Error)
		}

		remotePage := PlayerPage{
			Domain:       "https://beginworld.website-us-east-1.linodeobjects.com",
			Extension:    ".html",
			CoolPoints:   uint(p.CoolPoints),
			StreetCred:   uint(p.StreetCred),
			Lovers:       lovers,
			CommandCount: cmdCount,
			Commands:     cmds,
		}
		buildFile := fmt.Sprintf("build/users/%s.html", user)
		f, err := os.Create(buildFile)
		if err != nil {
			fmt.Printf("\nError Creating Build File: %s", err)
			return
		}
		err = user_tmpl.Execute(f, remotePage)
		if err != nil {
			fmt.Printf("err = %+v\n", err)
		}
	}
}
