## Goals

## Weekly Goals

# Bugs

- Some themes songs are being returned from !buy maybe
- check if sound already exists, and warn the user
- Look at party joined

## Small Todos

- Countdown on the cat timer
- !buy all
- Migrate the bots over and have them ignored for jester stuff
  - Build bot logic into more place and link bots to their owners
- Keep track of valid guess for pokemon
  -> also tell the people if the guess was even valid
- Build an admin channel to be able to silence any sounds easily
- Allow chat to control audio sync/desync

## Ideas

- Theme Song Guessing Game
- Investing in Sounds

### Linode Next Steps

- Instance
- Generate SSH Key
- Create Packer instance

## House Keeping

- Look at Approvals
- Migration to find what sounds aren't in the linode bucket
- Migration to find soundeffects that don't have corresponding filenames
- Migration to Clean Up Mana and money

## Jokes

- We have a list of the OG 150 pokemon in our system allready, including sounds,
  - We are gathering the images
  - we can grab a all the sources, and filter, by pokemon name
    to a get of all pokemon sources, for a random pokemon method
    .....then we add dynamic filters to black the pokemon all
    out....and finally rebuild Whose that pokemon,
    on OBS
