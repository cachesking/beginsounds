package obs

import (
	"fmt"
	"io/ioutil"
	"log"
	"sync"
	"testing"
	"time"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/pkg/chat"
)

func TestSceneSaver(t *testing.T) {
	c := obsws.Client{Host: "localhost", Port: 4444}
	var mutex = &sync.Mutex{}

	// You can use log.Println with these flags `log.SetFlags(log.LstdFlags | log.Lshortfile` that way you can also have date attached to each log

	// logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	logger := log.New(ioutil.Discard, "obsws2 ", log.LstdFlags)
	client := OBSClient{
		Client: &c,
		Mutex:  mutex,
		Logger: logger,
	}
	// Quick Dumb Theory
	// When you query a filters default settings
	// you get nothing back
	// s := FetchFilterSettings(&client, "tswift", "greenscreen")
	// s := FetchFilterSettings(&client, "922", "transform")
	// fmt.Printf("s = %+v\n", s)
	// <-time.NewTimer(time.Second * 2).C
	return
	sources, _ := SourcesByVisibility(&client, MemeScene)

	for _, source := range sources {
		fmt.Println("source: ", source)
		MemeInfo(&client, source)
	}
}

// !find sunbegin|!move sunbegin 881 167|!scale sunbegin 0.87|!rotate sunbegin 0
func TestChatExploder(t *testing.T) {
	fmt.Println("START chatExploder")
	name := "young.thug"
	msg := "!reset clippy | !slide clippy"
	cm := chat.ChatMessage{
		PlayerName: name,
		Message:    msg,
	}

	msgs := make(chan chat.ChatMessage, 1000)
	msgs <- cm

	results := chatExploder(msgs)
	timer := time.NewTimer(time.Millisecond * 150)

	select {
	case <-timer.C:
		t.Errorf("Didn't get message")
	case res1 := <-results:
		if res1.Message != "!reset clippy" {
			t.Errorf("Wrong Message: %s", res1.Message)
		}
		res2 := <-results
		if res2.Message != "!slide clippy" {
			t.Errorf("Wrong Message: %s", res2.Message)
		}

	}

}
