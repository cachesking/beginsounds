package info_router

import (
	"context"
	"fmt"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request"
	"gitlab.com/beginbot/beginsounds/pkg/stream_jester"
	"gitlab.com/beginbot/beginsounds/pkg/website_generator"
	"gorm.io/gorm"
)

// Route routes messages around finding user information
func Route(
	ctx context.Context,
	db *gorm.DB,
	msgs <-chan chat.ChatMessage,
) <-chan string {
	chatMsgs := make(chan string)

	go func() {
		defer close(chatMsgs)

		for msg := range msgs {
			parsedCmd := msg.ParsedCmd

			switch parsedCmd.Name {
			case "wtf":
				unapproved, _ := soundeffect_request.Unapproved(db)
				jester, _ := stream_jester.CurrentJester(db)

				var p *player.Player
				if jester.PlayerID != 0 {
					p = player.FindByID(db, jester.PlayerID)
				}

				wtf := fmt.Sprintf(
					"Chaos Mode: %t | Jester: %s | Unapproved: %d",
					jester.ChaosMode,
					p.Name,
					len(unapproved),
				)
				chatMsgs <- wtf
			case "help":
				// We should regenerate here
				chatMsgs <- "http://www.beginworld.exchange/help.html"
				website_generator.CreateHelpPage()
				website_generator.SyncPage("help")

				// This will show visible memes

			}
		}

	}()

	return chatMsgs
}
