package soundboard

import (
	"fmt"
	"log"
	"time"

	"github.com/veandco/go-sdl2/mix"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/beginbot/beginsounds/pkg/audio_request"
	// "gitlab.com/beginbot/beginsounds/pkg/audio_request"
)

// I could have an admin channel
// we are listening on
// if we send a stop message to this channel
// we cut the sound off
// very simple
// The time is for how long until you are cut off
func PlayAudio(
	audioRequest audio_request.AudioRequest,
	timer *time.Timer,
	noMemes <-chan bool,
) bool {
	mix.VolumeMusic(1)

	var success bool

	if err := sdl.Init(sdl.INIT_AUDIO); err != nil {
		log.Println("Error initializing SDL: ", err)
		return success
	}

	defer sdl.Quit()

	if err := mix.Init(mix.INIT_MP3); err != nil {
		log.Println("Error initializing Mix: ", err)
		return success
	}

	defer mix.Quit()

	if err := mix.OpenAudio(22050, mix.DEFAULT_FORMAT, 2, 4096); err != nil {
		log.Println(err)
		log.Println("Error Opening Audio: ", err)
		return success
	}
	defer mix.CloseAudio()

	const VOLUME = 10
	mix.VolumeMusic(VOLUME)

	// This is what actually plays the sound
	music, err := mix.LoadMUS(audioRequest.Path)
	if err != nil {
		log.Println(err)
		return success
	}

	ticker := time.NewTicker(100 * time.Millisecond)
	err = music.Play(1)
	if err != nil {
		log.Println(err)
		return success
	}
	defer ticker.Stop()

	success = true

	fmt.Printf("Playing: %+v\n", audioRequest.Name)
PlayingAudioLoop:
	for {
		select {
		case <-noMemes:
			music.Free()
			break PlayingAudioLoop
		case <-timer.C:
			music.Free()
			break PlayingAudioLoop
		default:
		}

		if !mix.PlayingMusic() {
			music.Free()
			break
		}

		<-ticker.C
	}

	return success
}
