package player

import (
	"fmt"
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestFind(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	sc := Find(db, "notarealuser")
	if sc.ID != 0 {
		t.Error("WE are finding non-existent users")
	}
	CreatePlayerFromName(db, "realuser")
	sc = Find(db, "realuser")
	if sc.ID == 0 {
		t.Error("We are not Finding the User")
	}
}

func TestCreatePlayerFromName(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	CreatePlayerFromName(db, "begin")
	count := Count(db)
	if count != 1 {
		t.Errorf("We should have created player %d", count)
	}
}

// This should take in a Command as well
func TestFindOrCreate(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)

	count := Count(db)
	if count != 0 {
		t.Errorf("We have the wrong players count %d", count)
	}
	// p1 := SavePlayer(db, "begin")
	p1 := FindOrCreate(db, "syoonee")
	fmt.Printf("p1 = %+v\n", p1)

	count = Count(db)
	if count != 1 {
		t.Errorf("We have the wrong players count %d", count)
	}

	p2 := FindOrCreate(db, "syoonee")
	count = Count(db)
	if count != 1 {
		t.Errorf("We have the wrong players count %d", count)
	}

	if p1.ID == 0 {
		t.Error("We aren't returning IDs", p1.ID)
	}

	if p1.ID != p2.ID {
		t.Errorf("We are supposed get the same user! %d %s", p1.ID, p2)
	}
}
