package obs

import (
	obsws "github.com/davidbegin/go-obs-websocket"
)

// PrimaryScene the main scene you stream from
var PrimaryScene = "primary"

// MemeScene all the viewer added memes
var MemeScene = "viewer_memes"

// ChangeScene simple call to change the OBS Scene
func ChangeScene(client *OBSClient, sceneName string) {
	req := obsws.NewSetCurrentSceneRequest(sceneName)
	client.execOBSCommand(&req)
}
