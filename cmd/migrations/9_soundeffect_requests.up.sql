CREATE TABLE public.soundeffect_requests (
    id SERIAL,
    name character varying(255) NOT NULL,
    url character varying(255) NOT NULL,
    approved bool,
    requester_id int references players(id),
    approver_id int references players(id),
    start_time character varying(255),
    end_time character varying(255),
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    deleted_at TIMESTAMP WITHOUT TIME ZONE,
    CONSTRAINT soundeffect_requests_pkey PRIMARY KEY (id)
);
