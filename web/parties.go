package main

import (
	"net/http"

	"gitlab.com/beginbot/beginsounds/pkg/party"
)

type PartiesPage struct {
	Domain    string
	Extension string
	Parties   []party.PartiesAndMembers
}

func PartiesHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)

	// parties := party.AllParties(db)
	parties := party.AllPartiesAndMembers(db)
	page := PartiesPage{
		Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension: ".html",
		Parties:   parties,
	}

	partiesTmpl.Execute(w, page)
}
