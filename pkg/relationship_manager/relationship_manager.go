package relationship_manager

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"strings"

	"gitlab.com/beginbot/beginsounds/pkg/chat"
	"gitlab.com/beginbot/beginsounds/pkg/player"
	"gorm.io/gorm"
)

type PlayerLover struct {
	PlayerID int
	LoverID  int
}

// Handle no Lovers
func RandomLover(db *gorm.DB, playerID int) (string, error) {
	lovers := LovedNames(db, playerID)
	if len(lovers) == 0 {
		return "", errors.New("No Lovers")
	}
	randomIndex := rand.Intn(len(lovers))
	return lovers[randomIndex], nil
}

// How many people a Player Loves
func LovedCount(db *gorm.DB, playerID int) int64 {
	var count int64
	res := db.Table("players_lovers").Where("lover_id = ?", playerID).Count(&count)
	if res.Error != nil {
		fmt.Println("Error Finding Loved Count", res.Error)
	}
	return count
}

// How many people a Love a Player
func LoverCount(db *gorm.DB, playerID int) int64 {
	var count int64
	res := db.Table("players_lovers").Where("player_id = ?", playerID).Count(&count)
	if res.Error != nil {
		fmt.Println("Error Finding Lover Count", res.Error)
	}
	return count
}

func LoverNames(db *gorm.DB, playerId int) []string {
	var results []string

	tx := db.Table("players").Raw(`
		SELECT p.name FROM players p
		INNER JOIN players_lovers pl ON p.ID = pl.player_id
		WHERE pl.lover_id = ?`, playerId).Scan(&results)

	if tx.Error != nil {
		fmt.Printf("Error LoverNames: %+v\n", tx.Error)
	}

	return results
}

func LovedNames(db *gorm.DB, playerId int) []string {
	var results []string

	tx := db.Table("players").Raw(`
		SELECT p.name FROM players p
		INNER JOIN players_lovers pl ON p.ID = pl.lover_id
		WHERE pl.player_id = ?`, playerId).Scan(&results)

	if tx.Error != nil {
		fmt.Printf("Error find Lovers: %+v\n", tx.Error)
	}

	return results
}

func Manage(
	ctx context.Context,
	db *gorm.DB,
	msgs <-chan chat.ChatMessage,
) <-chan string {

	results := make(chan string)

	go func() {
		defer close(results)

	MsgLoop:
		for msg := range msgs {
			select {
			case <-ctx.Done():
			default:
				parsedCmd := msg.ParsedCmd

				switch parsedCmd.Name {
				case "lovers":
					lovers := strings.Join(LovedNames(db, msg.PlayerID), ", ")
					m1 := fmt.Sprintf("@%s Loves: %+v", msg.PlayerName, lovers)
					loved := strings.Join(LoverNames(db, msg.PlayerID), ", ")
					if len(loved) == 0 {
						results <- m1
						continue MsgLoop
					}
					m2 := fmt.Sprintf("%v | @%v Loves: @%s", m1, loved, msg.PlayerName)
					results <- m2

				case "love":
					if parsedCmd.TargetUser != "" {
						p := player.Find(db, parsedCmd.TargetUser)
						pl := PlayerLover{PlayerID: msg.PlayerID, LoverID: p.ID}
						lovers := LoverCount(db, msg.PlayerID)
						loved := LovedCount(db, msg.PlayerID)
						if p.ID == msg.PlayerID {
							results <- fmt.Sprintf("@%s You can't love yourself", msg.PlayerName)
							continue MsgLoop
						}
						if lovers > loved {
							results <- fmt.Sprintf("@%s Is not Loved enough to love @%s | %d/%d",
								msg.PlayerName, parsedCmd.TargetUser, lovers, loved)
							continue MsgLoop
						}

						res := db.Table("players_lovers").Create(&pl)

						if res.Error != nil {
							results <- fmt.Sprintf("@%s already loves @%s",
								msg.PlayerName, parsedCmd.TargetUser)
							continue MsgLoop
						}
						results <- fmt.Sprintf("@%s Now Loves | @%s", msg.PlayerName, parsedCmd.TargetUser)
					}

				case "hate":
					if parsedCmd.TargetUser != "" {
						p := player.Find(db, parsedCmd.TargetUser)
						pl := PlayerLover{PlayerID: msg.PlayerID, LoverID: p.ID}
						tx := db.Table("players_lovers").
							Where("player_id = ? AND lover_id = ?", msg.PlayerID, p.ID).Delete(&pl)

						if tx.Error != nil {
							results <- fmt.Sprintf("Error @%s Hating @%s | %v+\n",
								msg.PlayerName, parsedCmd.TargetUser, tx.Error)
							continue MsgLoop
						}

						results <- fmt.Sprintf("@%s No Longer Loves @%s",
							msg.PlayerName, parsedCmd.TargetUser)
						continue MsgLoop
					}
				}
			}
		}
	}()
	return results
}
