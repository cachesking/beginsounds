CREATE TABLE public.scavenger_secrets (
    id SERIAL,
    code character varying(255) NOT NULL,
    access_key_id int references access_keys(id) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT scavenger_secrets_pkey PRIMARY KEY (id)
);
