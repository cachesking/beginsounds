CREATE TABLE public.packs_players (
    id SERIAL,

    pack_id int references packs(id),
    player_id int references players(id),

    created_at timestamp with time zone DEFAULT now() NOT NULL,

    CONSTRAINT packs_players_unique UNIQUE(pack_id, player_id),
    CONSTRAINT packs_players_pkey PRIMARY KEY (id)
);
