package reporter

import (
	"fmt"
	"testing"
)

func TestSplittingLongMessages(t *testing.T) {
	// shortMsg := ""
	longMsg := `Vim has a special mode to speedup the edit-compile-edit cycle.  This is
inspired by the quickfix option of the Manx's Aztec C compiler on the Amiga.
The idea is to save the error messages from the compiler in a file and use Vim
to jump to the errors one by one.  You can examine each problem and fix it,
without having to remember all the error messages.

In Vim the quickfix commands are used more generally to find a list of
positions in files.  For example, |:vimgrep| finds pattern matches.  You can
use the positions in a script with the |getqflist()| function.  Thus you can
do a lot more than the edit/compile/fix cycle!`

	res := msgSplitter(longMsg)
	if len(res) != 2 {
		t.Errorf("Didn't split Message properly: %d", len(res))
	}

	fmt.Printf("len(longMsg) = %+v\n", len(longMsg))

}
