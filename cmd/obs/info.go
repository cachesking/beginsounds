package obs

import (
	"fmt"
	"strings"

	obsws "github.com/davidbegin/go-obs-websocket"
	"gitlab.com/beginbot/beginsounds/pkg/memes"
)

// MemeInfo Fetch info about a meme
func MemeInfo(
	client *OBSClient,
	sourceName string,
) memes.Meme {

	sceneName := "viewer_memes"
	resp := FetchSourceSettings(client, sceneName, "scene")
	settings := resp.SourceSettings
	items, _ := settings["items"]
	is := items.([]interface{})

	for _, item := range is {
		switch item := item.(type) {
		case map[string]interface{}:
			name := item["name"].(string)
			pos := item["pos"].(map[string]interface{})
			scaleMap := item["scale"].(map[string]interface{})
			rotation := item["rot"].(float64)

			x := pos["x"].(float64)
			y := pos["y"].(float64)
			scale := scaleMap["x"].(float64)

			if name == sourceName {
				return memes.Meme{
					Name:     name,
					X:        x,
					Y:        y,
					Scale:    scale,
					Rotation: rotation,
				}
			}
		}
	}
	return memes.Meme{}
}

// We could make this filter on visible
// only return visible sources
// TODO: make take in scene
func SourcesByVisibility(
	client *OBSClient,
	scene string,
) ([]string, []string) {
	visibleSources := []string{}
	invisibleSources := []string{}

	sources := FetchSources(client)
	for _, s := range sources {
		sourceName := s["name"]
		typeKind, _ := s["typeId"]

		name := sourceName.(string)
		kind := typeKind.(string)

		settings := FetchSceneItemSettings(client, scene, name, kind)
		if settings.Visible {
			visibleSources = append(visibleSources, name)
		} else {
			invisibleSources = append(invisibleSources, name)
		}
	}

	return visibleSources, invisibleSources
}

// FetchSources - Fetch a list of all the sources
func FetchSources(client *OBSClient) []map[string]interface{} {
	req := obsws.NewGetSourcesListRequest()
	resp, _ := client.execOBSCommand(&req)
	r := resp.(obsws.GetSourcesListResponse)
	return r.Sources
}

// ImageSources gathers only the image sources
func ImageSources(client *OBSClient) string {
	images := []string{}
	sources := FetchSources(client)
	for _, source := range sources {
		if source["typeId"] == "image_source" {
			name := source["name"].(string)
			images = append(images, name)
		}
	}

	return strings.Join(images, " ")
}

func FetchSceneItemSettings(
	client *OBSClient,
	sceneName string,
	sourceName string,
	sourceType string,
) *obsws.GetSceneItemPropertiesResponse {

	item := map[string]interface{}{
		"name": sourceName, "type": "input", "typeId": sourceType,
	}
	req := obsws.NewGetSceneItemPropertiesRequest(
		sceneName, item, sourceName, 0,
	)
	resp, _ := client.execOBSCommand(&req)
	r := resp.(obsws.GetSceneItemPropertiesResponse)
	return &r
}

// FetchSourceSettings fetch source settings from OBS
func FetchSourceSettings(
	client *OBSClient,
	sourceName string,
	sourceType string,
) *obsws.GetSourceSettingsResponse {

	req := obsws.NewGetSourceSettingsRequest(sourceName, sourceType)
	resp, _ := client.execOBSCommand(&req)
	r := resp.(obsws.GetSourceSettingsResponse)
	return &r
}

func FetchFilterSettings(
	client *OBSClient,
	sourceName string,
	filterName string,
) map[string]interface{} {

	req := obsws.NewGetSourceFilterInfoRequest(sourceName, filterName)
	resp, err := client.execOBSCommand(&req)
	if err != nil {
		fmt.Println("Error Fetching Filter Settings: ", err)
	}
	r := resp.(obsws.GetSourceFilterInfoResponse)
	return r.Settings
}
