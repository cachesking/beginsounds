package database

import (
	"fmt"
	"os"

	_ "github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

func CreateDBConn(dbname string) *gorm.DB {
	var dsn string
	// This is for Gitlab CI, or within a Docker context
	if os.Getenv("DOCKER") != "" {
		dsn = fmt.Sprintf("host=postgres user=postgres dbname=%s port=5432 sslmode=disable", dbname)
	} else {
		dsn = fmt.Sprintf("user=postgres dbname=%s port=5432 sslmode=disable", dbname)
	}

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		// Logger: logger.Default.LogMode(logger.Error),
		Logger: logger.Default.LogMode(logger.Silent),
	})

	// I wonder if we just don't want to panic here anymore??
	if err != nil {
		panic(err)
	}

	fmt.Println("Connecting to DB: ", dbname)

	return db
}
