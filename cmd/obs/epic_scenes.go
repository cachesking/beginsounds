package obs

import (
	"fmt"
	"sync"
	"time"
)

// CloneThang takes a source and clones it in the scene
func CloneThang(
	client *OBSClient,
	sceneName string,
	baseSource string,
	count int,
) {

	baseName := baseSource

	var wg sync.WaitGroup
	wg.Add(count)

	x := 0.0

	for i := 1; i < count+1; i++ {
		go func(c *OBSClient, w *sync.WaitGroup, num int, xValue *float64) {
			sourceKind := "streamfx-source-mirror"
			newSourceName := fmt.Sprintf("%s-%d", baseName, num)

			settings := map[string]interface{}{
				"Source.Mirror.Source": baseName,
			}

			CreateSource(c, sceneName, newSourceName, sourceKind, settings)
			AddOutlineFilter(c, sceneName, newSourceName)
			AddTransformFilter(c, sceneName, newSourceName)
			AddNormieTransformFilter(c, sceneName, newSourceName)
			// This timer is too long!
			<-time.NewTimer(time.Second * 2).C
			UpdateSourceSettings(c, newSourceName, sourceKind, settings)

			slideSettings := map[string]interface{}{
				"start_x": 0.0,
				"end_x":   1365.0,
				"y":       *xValue,
			}
			SlideSource(client, sceneName, newSourceName, slideSettings)

			*xValue += 50.0
			wg.Done()
		}(client, &wg, i, &x)

	}
	wg.Wait()
}

func zoomseal(client *OBSClient) {
	toggleTransform(client, "922", true)
	toggleTransform(client, "Screen", true)
	toggleTransform(client, "Chat", true)
	ToggleSource(client, PrimaryScene, "sealspin", true)
	ZoomAndSpin(client, "Screen", 5)
	ZoomAndSpin(client, "Chat", 5)
	ZoomAndSpin(client, "922", 5)
	PulsatingHighlight(client, "sealspin", 50, 7, true)
	ZoomAndSpin(client, "sealspin", 4)
}

func beginboy(client *OBSClient) {
	ToggleSource(client, MemeScene, "beginboy", true)
	beginboyRiseSettings := map[string]interface{}{
		"start_y": 750.0,
		"end_y":   300.0,
		"x":       0.0,
	}
	RiseSource(client, MemeScene, "beginboy", beginboyRiseSettings)
	ToggleSource(client, MemeScene, "artmattcardflat", true)
	ZoomFieldOfView(client, "artmattcardflat")
	ToggleSource(client, MemeScene, "artmattfloppycard", true)
	AppearFromThinAir(client, "artmattfloppycard")
	ToggleSource(client, PrimaryScene, "JesterCode", true)
	PulsatingHighlight(client, "JesterCode", 50, 7, true)
	<-time.NewTimer(time.Second * 5).C
	ReturnToNormie(client)
}
