package colorscheme

import (
	"testing"

	"gitlab.com/beginbot/beginsounds/pkg/database"
	"gitlab.com/beginbot/beginsounds/pkg/test_support"
)

func TestWinningColor(t *testing.T) {
	db := database.CreateDBConn("test_beginsounds3")
	test_support.ClearDb(db)
	csv := ColorschemeVote{Username: "young.thug", Theme: "slimey-ysl4lfye"}
	db.Save(&csv)
	res := NewWinningColor(db)

	if res.Theme != "slimey-ysl4lfye" {
		t.Skipf("Didn't find the Winning Color! %v", res)
	}

	all := All(db)
	if all[0].Theme != "slimey-ysl4lfye" {
		t.Skipf("Error Finding all the Colors: %v", all[0].Theme)
	}
}
