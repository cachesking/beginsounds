package obs

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type SourceToggleConfig struct {
	Scene  string
	Source string
	Toggle bool
}

// ToggleSources shortcuts to toggle various sources on and off
var ToggleSources = map[string]SourceToggleConfig{
	"!catjam": {
		Scene:  MemeScene,
		Source: "shia",
		Toggle: true,
	},
	"!lbbegin": {
		Scene:  PrimaryScene,
		Source: "922-left-bottom",
		Toggle: true,
	},
	"!ltbegin": {
		Scene:  PrimaryScene,
		Source: "922-left-top",
		Toggle: true,
	},
	"!rtbegin": {
		Scene:  PrimaryScene,
		Source: "922-right-top",
		Toggle: true,
	},
	"!freebegin": {
		Scene:  MemeScene,
		Source: "cage",
		Toggle: false,
	},
	"!cage": {
		Scene:  MemeScene,
		Source: "cage",
		Toggle: true,
	},
}

// ObsCommand represents an OBS command
type ObsCommand struct {
	Name  string `json:"name"`
	Type  string `json:"type"`
	Scene string `json:"scene"`
}

func (o *ObsCommand) String() string {
	return fmt.Sprintf(
		"ObsCommand<Name: %s Type: %s Scene: %s>",
		o.Name,
		o.Type,
		o.Scene,
	)
}

func parseConfig(configFile string) []ObsCommand {
	var results []ObsCommand
	f, err := ioutil.ReadFile(configFile)
	if err != nil {
		panic(err)
	}
	if err := json.Unmarshal(f, &results); err != nil {
		panic(err)
	}
	return results
}
