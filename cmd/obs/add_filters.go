package obs

import (
	"strconv"

	obsws "github.com/davidbegin/go-obs-websocket"
)

// FilterSetting - Represents the values needed to Create of Set Filter Settings
type FilterSetting struct {
	Name       string
	FilterType string
	Settings   map[string]interface{}
}

// AddColorFadeFilter - Adds filter fade from one color to another from the color_correction
//
// TODO: we might want to extract out settings for color_correction
func AddColorFadeFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	settings := FilterSetting{
		Name:       "colorfade",
		FilterType: "move_value_filter",
		Settings: map[string]interface{}{
			"brightness":     0,
			"color":          Green,
			"contrast":       0,
			"duration":       10000,
			"filter":         "color_correction",
			"gamma":          0,
			"hue_shift":      0,
			"opacity":        64,
			"saturation":     0,
			"single_setting": false,
			"start_trigger":  5,
			"stop_trigger":   4,
			"value_type":     0,
		},
	}

	return addFilter(
		client,
		sceneName,
		sourceName,
		settings,
	)
}

// AddColorCorrectionFilter - Add an OBS color correction filter
func AddColorCorrectionFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	settings := FilterSetting{
		Name:       "color_correction",
		FilterType: "color_correction",
		Settings: map[string]interface{}{
			"brightness": 0,
			"color":      4.278190335e+09,
			"contrast":   0,
			"duration":   10000,
			"filter":     "color_correction",
			"gamma":      0,
			"hue_shift":  0,
			"opacity":    32,
			"saturation": 0,
		},
	}

	return addFilter(
		client,
		sceneName,
		sourceName,
		settings,
	)
}

// AddNormieTransformFilter - Add the filter to return all
//
// TODO: We need to figure out how to make this work
// without having toggle the settings manually in OBS
// AddOutlineFilter adds a filter to a source
func AddNormieTransformFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	settings := FilterSetting{
		Name:       "normie_transform",
		FilterType: "move_value_filter",
		Settings: map[string]interface{}{
			"Filter.Transform.Camera.FieldOfView": 90,
			"Filter.Transform.Position.X":         0,
			"Filter.Transform.Position.Y":         0,
			"Filter.Transform.Position.Z":         0,
			"Filter.Transform.Rotation.X":         0,
			"Filter.Transform.Rotation.Y":         0,
			"Filter.Transform.Rotation.Z":         0,
			"Filter.Transform.Scale.X":            100,
			"Filter.Transform.Scale.Y":            100,
			"Filter.Transform.Shear.X":            0,
			"Filter.Transform.Shear.Y":            0,
			"duration":                            3000,
			"filter":                              "transform",
			"setting_float":                       0,
			"single_setting":                      false,
			"start_trigger":                       0,
			"value_type":                          0,
		},
	}

	return addFilter(
		client,
		sceneName,
		sourceName,
		settings,
	)
}

// AddGreenScreenFilter - Adds a Chroma Key Source in OBS
func AddGreenScreenFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {
	settings := FilterSetting{
		Name:       "greenscreen",
		FilterType: "chroma_key_filter",
		Settings: map[string]interface{}{
			"filter": "greenscreen",
		},
	}

	return addFilter(
		client,
		sceneName,
		sourceName,
		settings,
	)
}

// AddTransformFilter adds a filter to a source
func AddTransformFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	settings := FilterSetting{
		Name:       "transform",
		FilterType: "streamfx-filter-transform",
		Settings: map[string]interface{}{
			"Backlight Compensation":              0,
			"Brightness":                          0,
			"Contrast":                            0,
			"Exposure (Absolute)":                 0,
			"Filter.Transform.Camera":             1,
			"Filter.Transform.Camera.FieldOfView": 90,
			"Filter.Transform.Position.X":         0,
			"Filter.Transform.Position.Y":         0,
			"Filter.Transform.Position.Z":         0,
			"Filter.Transform.Rotation.X":         0,
			"Filter.Transform.Rotation.Y":         0,
			"Filter.Transform.Rotation.Z":         0,
			"Filter.Transform.Scale.X":            100,
			"Filter.Transform.Scale.Y":            100,
			"Filter.Transform.Shear.X":            0,
			"Filter.Transform.Shear.Y":            0,
			"Focus (absolute)":                    0,
			"Gain":                                0,
			"Pan (Absolute)":                      0,
			"Saturation":                          0,
			"Sharpness":                           0,
			"Tilt (Absolute)":                     0,
			"White Balance Temperature":           0,
			"Zoom Absolute":                       0,
		},
	}

	return addFilter(
		client,
		sceneName,
		sourceName,
		settings,
	)
}

// AddOutlineFilter adds a filter to a source
func AddOutlineFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
) error {

	color, _ := strconv.ParseFloat(Purple, 64)
	settings := FilterSetting{
		Name:       "outline",
		FilterType: "streamfx-filter-sdf-effects",
		Settings: map[string]interface{}{
			"Filter.SDFEffects.Outline.Color": color,
		},
	}

	return addFilter(
		client,
		sceneName,
		sourceName,
		settings,
	)
}

func addFilter(
	client *OBSClient,
	sceneName string,
	sourceName string,
	filterSettings FilterSetting,
) error {

	req := obsws.NewAddFilterToSourceRequest(
		sourceName,
		filterSettings.Name,
		filterSettings.FilterType,
		filterSettings.Settings,
	)
	_, err := client.execOBSCommand(&req)
	return err
}
