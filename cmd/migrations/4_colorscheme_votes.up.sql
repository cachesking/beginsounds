CREATE TABLE public.colorscheme_votes(
    id SERIAL,
    username character varying(255) NOT NULL,
    theme character varying(255) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    CONSTRAINT colorscheme_votes_pkey PRIMARY KEY (id)
);
