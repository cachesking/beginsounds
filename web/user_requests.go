package main

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/beginbot/beginsounds/pkg/media_request"
	"gitlab.com/beginbot/beginsounds/pkg/soundeffect_request"
)

// MediaRequests represents information about
// 		unapproved media requests
type MediaRequests struct {
	Domain        string
	Extension     string
	MediaRequests []*media_request.MediaRequest
}

type UserRequestsPage struct {
	Domain       string
	Extension    string
	UserRequests []*soundeffect_request.SoundeffectRequest
}

type MediaRequestsPage struct {
	Domain     string
	Extension  string
	Unapproved []*media_request.MediaRequest
	// Videos []*media_request.MediaRequest
	// Videos []*media_request.MediaRequest
	// Images []*media_request.MediaRequest
}

func MediaRequestsHandler(w http.ResponseWriter, r *http.Request) {

	// images := media_request.UnapprovedImages(db)
	// videos := media_request.UnapprovedVideos(db)

	unapproved := media_request.Unapproved(db)

	remotePage := MediaRequestsPage{
		Unapproved: unapproved,
		// Images:    images,
		// Videos:    videos,
		Domain:    "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension: ".html",
	}

	buildFile := fmt.Sprintf("build/media_requests.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Media Reuqests file: %s", err)
		return
	}
	err = mediaRequestsTmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := MediaRequestsPage{
		Domain:     "http://localhost:1992",
		Unapproved: unapproved,
		// Images: images,
		// Videos: videos,
	}
	w.WriteHeader(http.StatusOK)
	mediaRequestsTmpl.Execute(w, page)
}

func UserRequestsHandler(w http.ResponseWriter, r *http.Request) {
	// We need all unapproved requests
	sfxs, _ := soundeffect_request.Unapproved(db)

	// We could include other requests here
	remotePage := UserRequestsPage{
		UserRequests: sfxs,
		Domain:       "https://beginworld.website-us-east-1.linodeobjects.com",
		Extension:    ".html",
	}
	buildFile := fmt.Sprintf("build/user_requests.html")
	f, err := os.Create(buildFile)
	if err != nil {
		fmt.Printf("\nError Creating Build File: %s", err)
		return
	}
	err = userRequestsTmpl.Execute(f, remotePage)
	if err != nil {
		fmt.Printf("err = %+v\n", err)
	}

	page := UserRequestsPage{
		Domain:       "http://localhost:1992",
		UserRequests: sfxs,
	}
	w.WriteHeader(http.StatusOK)
	userRequestsTmpl.Execute(w, page)
}
